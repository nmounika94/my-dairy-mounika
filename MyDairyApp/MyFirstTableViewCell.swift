//
//  MyFirstTableViewCell.swift
//  MyDairyApp
//
//  Created by Mounika Nerella on 8/21/17.
//  Copyright © 2017 Mounika Nerella. All rights reserved.
//

import UIKit

class MyFirstTableViewCell: UITableViewCell {

    @IBOutlet weak var imageLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
