//
//  ThirdViewController.swift
//  MyDairyApp
//
//  Created by Mounika Nerella on 8/20/17.
//  Copyright © 2017 Mounika Nerella. All rights reserved.
//

import UIKit
protocol  whenEntryDone: class {
    func entryofData()
}
class ThirdViewController: UIViewController {
    var newImage = UIImage()
    @IBOutlet weak var myThoughtsEntry: UITextView!
    @IBOutlet weak var imageseen: UIImageView!
    @IBOutlet weak var textLabel: UILabel!
    var  tblViewDetails: Details? = nil
    var didEdit: Bool = false
    var path: String = ""
    weak var delegate: whenEntryDone?
    @IBOutlet weak var nextPge: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
       textLabel?.text = tblViewDetails?.Title
       myThoughtsEntry?.text = tblViewDetails?.descp
        grabbingImage()
        // Do any additional setup after loading the view.
    }
    func grabbingImage(){
        var pathImage = path
        pathImage.removeSubrange(pathImage.range(of: ".plist")!)
       // var pathImage = path
        let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
        let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
        if let dirPath          = paths.first
        {
            let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent("Images/\(pathImage).png")
            self.imageseen.image    = UIImage(contentsOfFile: imageURL.path)
            
        }
//        ManagingFiles.readImageFromPath(path: pathImage) {(imgData) in
//            DispatchQueue.main.async {
//                self.imageseen.image = UIImage(data:imgData  as Data, scale: 1.3)
//            }
//        }
        
    }
    

    @IBAction func previousPage(_ sender: UIButton) {
        if didEdit == true{
        tblViewDetails?.descp = myThoughtsEntry.text
            ManagingFiles.writeToFile(val: myThoughtsEntry.text, path: path, completionHandler: {_ in
                DispatchQueue.main.async {
                    self.delegate?.entryofData()
                }
            })
            
        }
        if myThoughtsEntry.text != ""{
            navigationController?.popViewController(animated: true)
        }
            else{
                let alertController = UIAlertController(title: "Alert", message: "Please Fill all Fields", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                alertController.addAction(okAction)
                present(alertController, animated: true, completion: nil)
            }
        
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */


