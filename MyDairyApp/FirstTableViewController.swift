//
//  FirstTableViewController.swift
//  MyDairyApp
//
//  Created by Mounika Nerella on 8/20/17.
//  Copyright © 2017 Mounika Nerella. All rights reserved.
//

import UIKit

class FirstTableViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,addentry,whenEntryDone,UISearchResultsUpdating {
    
    @IBOutlet weak var tblView: UITableView!
    let searchController = UISearchController(searchResultsController: nil)
    var dairyinfo = [Details]()
    var pathArray = [String]()
    var resultsArray = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        gettingPathNames()
        searchController.searchResultsUpdater = self
        tblView.tableHeaderView = searchController.searchBar
        tblView.reloadData()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        tblView.reloadData()
    }
    
    @IBAction func newPage(_ sender: UIBarButtonItem) {
    

        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        if let controller = storyBoard.instantiateViewController(withIdentifier: "SecondViewController") as? SecondViewController{
            
            controller.delegate = self
            present(controller,animated: true,completion: nil)
        }
    }
    
      func addNewEntry(fileName: String, data: String) {
        gettingPathNames()
        tblView.reloadData()
    }
    func updateSearchResults(for searchController: UISearchController) {
        resultsArray = pathArray.filter({ (title)->Bool in
            if title.localizedCaseInsensitiveContains(searchController.searchBar.text!){
                return true
            }
            return false
    })
        tblView.reloadData()
    }
    func gettingPathNames(){
        pathArray = [String]()
        ManagingFiles.readFileFromPath { (pathArray) in
            self.pathArray = pathArray
            DispatchQueue.main.async {
                self.tblView.reloadData()
            }
            
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchController.isActive && searchController.searchBar.text! != ""{
            return resultsArray.count
    }
        return pathArray.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        if let controller = storyBoard.instantiateViewController(withIdentifier: "ThirdViewController") as? ThirdViewController {
            var dict = NSMutableDictionary()
            var obj: Details?
            print(obj)
            var pathString: String
            if searchController.isActive && searchController.searchBar.text != "" {
                pathString = resultsArray[indexPath.row]
            }else{
                pathString = pathArray[indexPath.row]
                
            }
            ManagingFiles.readingDataFromPath(path: pathString, completionHandler: { (mutDict) in
                DispatchQueue.main.async {
                    dict = mutDict
                    obj = Details(titleName: dict["Title"] as! String, des: dict["Description"] as! String, imgs: dict["Image"] as! String)
                    controller.path = pathString
                    controller.tblViewDetails = obj
                    controller.delegate = self
                    self.navigationController?.pushViewController(controller, animated: true)
                    
                }
            })
            
            
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyFirstTableViewCell", for: indexPath) as? MyFirstTableViewCell
        let cellObj: String
        if searchController.isActive && searchController.searchBar.text! != "" {
           cellObj = resultsArray[indexPath.row]
        }else{
            cellObj = pathArray[indexPath.row]
            
        }
        cell?.titleLabel.text = cellObj
        
        return cell!
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    // MARK: - Table view data source

     
     
    
        

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

  
    // Override to support editing the table view.
     func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let deleteEntry = pathArray.remove(at: indexPath.row)
            ManagingFiles.deleteImage(path: deleteEntry, completionHandler: {
                DispatchQueue.main.async {
                    tableView.deleteRows(at: [indexPath], with: .fade)
                    
                }
            })
            // Delete the row from the data source
        }
    }
    
    func entryofData() {
        tblView.reloadData()
        
    }
    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
