//
//  ErrorHandling.swift
//  MyDairyApp
//
//  Created by Mounika Nerella on 8/22/17.
//  Copyright © 2017 Mounika Nerella. All rights reserved.
//

import Foundation
enum ErrorHandler: Error {
    case emptyDocumentDirectory
    case creatingDirectoryFailed
   case subPathsError
}
