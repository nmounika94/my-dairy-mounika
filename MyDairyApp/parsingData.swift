//
//  parsingData.swift
//  MyDairyApp
//
//  Created by Mounika Nerella on 8/22/17.
//  Copyright © 2017 Mounika Nerella. All rights reserved.
//

import Foundation
class parsingList{
    static func myPlistFilesAsUrl(_ url: URL) throws -> [String]{
        var fileName = [String] ()
        do{
            var subPath:[String]
        subPath = try FileManager.default.subpaths(atPath: url.path)!
            for files in subPath{
                if files.contains(".plist"){
                    fileName.append(files)
                }
            }
        }
        return fileName
    }
}
