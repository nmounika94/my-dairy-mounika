//
//  SecondViewController.swift
//  MyDairyApp
//
//  Created by Mounika Nerella on 8/20/17.
//  Copyright © 2017 Mounika Nerella. All rights reserved.
//

import UIKit
protocol  addentry : class{
    func addNewEntry(fileName: String, data: String)
}

class SecondViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate,UITextViewDelegate{
    
    var myFileurl: URL? = nil
    var file:String = ""
    var str: String = ""
    weak var delegate: addentry?
    @IBOutlet weak var myTitleInfo: UITextField!
    @IBOutlet weak var myImage: UIImageView!
    @IBOutlet weak var enterData: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    func addingAllInfo(title: String, descption:String, imageName: String) {
        file = title + ".plist"
        let theInfo = Details(titleName: title, des: descption, imgs: imageName )
       ManagingFiles.addDataToFile(dairy: theInfo, path: file)
            DispatchQueue.main.async {
                self.convertingImage()
            }
        }
        
    

    
    @IBAction func cancelBtn(_ sender: UIButton) {
    
        dismiss(animated: true, completion: nil)
    }
   
   
    @IBAction func addBtn(_ sender: UIButton) {
    
   
        if myTitleInfo.text != "" && enterData.text != "" && myImage.image != nil{
            addingAllInfo(title: myTitleInfo.text!, descption: enterData.text!, imageName: myTitleInfo.text!)
        }
            else {
                let alertController = UIAlertController(title: "Error", message: "Please Fill all Fields", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                alertController.addAction(okAction)
                present(alertController, animated: true, completion: nil)
           

        }
       
            }
    func convertingImage(){
        if myImage.image != nil{
            var data: Data
            data = UIImageJPEGRepresentation(myImage.image!, 1.5)!
            print(file)
            file.removeSubrange(file.range(of: ".plist")!)
            ManagingFiles.addingImageToFile(path: file, imageData: data as Data,completionHandler: {
                DispatchQueue.main.async {
                    self.delegate?.addNewEntry(fileName: self.myTitleInfo.text!, data: self.str)
                    self.dismiss(animated: true, completion: nil)
                    
                    
                }
            })
        }
        
    }
    @IBAction func PickImage(_ sender: UIButton) {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .photoLibrary
        let actionSheet = UIAlertController(title: "Photo Source", message: "Choose a source", preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: {(action:UIAlertAction) in imagePicker.sourceType = .camera
        self.present(imagePicker,animated: true, completion: nil)
        }))
        actionSheet.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: {(action:UIAlertAction) in imagePicker.sourceType = .photoLibrary}))
       
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel,handler: nil))
         present(imagePicker,animated: true, completion: nil)
    }
    func feildShouldReturn(_ textFeild: UITextField) -> Bool{
        if textFeild == myTitleInfo{
            enterData.becomeFirstResponder()
            return false
        
        }else{
            enterData.resignFirstResponder()
        }
        return true
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image1 = info[UIImagePickerControllerOriginalImage] as! UIImage
       myImage.image = image1
        dismiss(animated: true, completion: nil)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destinationViewController = segue.destination as! ThirdViewController
        destinationViewController.newImage = myImage.image!
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
